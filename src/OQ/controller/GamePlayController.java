/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OQ.controller;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import OQ.StartupConstants;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.util.Duration;

/**
 *
 * @author BunnyRailgun
 */
public class GamePlayController {
    
    public void handleGameInit(Stage primaryStage){
        Group root = new Group();
        Scene gameScene = new Scene(root);
        primaryStage.setScene(gameScene);
        Canvas canvas = new Canvas(primaryStage.getWidth(), primaryStage.getHeight());
        root.getChildren().add(canvas);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        double width = primaryStage.getWidth();
        double height = primaryStage.getHeight();
        //BACKGROUND
        Image bg = new Image(StartupConstants.PATH_IMAGES + "background.jpg");
        gc.drawImage(bg, 0, 0, width, height);
        //SHOW EMILIA
        Image emilia = new Image(StartupConstants.PATH_IMAGES + "emilia_standing.png");
        double emiliaPrefHeight = emilia.getHeight()*.07;
        double emiliaPrefWidth = emilia.getWidth()*.07;
        gc.drawImage(emilia, width/2, height-(emiliaPrefHeight+45), emiliaPrefWidth, emiliaPrefHeight);
        //DRAW SHEEP
        populateSheep(gc, width,height-(emiliaPrefHeight+200));
        primaryStage.show();
        
        //COUNT TIME
        canvas.setOnKeyPressed(new EventHandler<KeyEvent>() {
            
            @Override
            public void handle(KeyEvent event) {
                System.out.println(event.getCode());
            }
        });
    }
    public void populateSheep(GraphicsContext gc, double width, double height){
        Image sheep = new Image(StartupConstants.PATH_IMAGES + "sheep_happy.png");
        for(int i = 0; i < 20; i++){
            gc.drawImage(sheep, Math.random()*width, Math.random()*height, 100,100);
        }
    }
    public void populateSheepTop(GraphicsContext gc, double width){
        Image sheep = new Image(StartupConstants.PATH_IMAGES + "sheep_happy.png");
        for(int i = 0; i < 5; i++){
            gc.drawImage(sheep, Math.random()*width, 0, 100, 100);
        }
    }
    public void shiftDown(Canvas c){
        c.setTranslateY(2);
    }
    
}
