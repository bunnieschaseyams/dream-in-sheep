/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OQ.controller;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import OQ.StartupConstants;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;

/**
 *
 * @author BunnyRailgun
 */
public class TitleScreenController {
    GamePlayController gamePlayController = new GamePlayController();
    
    public void handleStartGame(Stage primaryStage, Button button){
        BorderPane sleepPrompt = new BorderPane();
        sleepPrompt.getStylesheets().add(StartupConstants.STYLE_SHEET_UI);
        sleepPrompt.getStyleClass().add("sleep_dialog");
        VBox sleepBox = new VBox();
        Label sleepQuestion = new Label("How many hours of sleep did you get last night?");
        sleepQuestion.getStyleClass().add("sleep_question");
        
        ObservableList<String> options = 
           FXCollections.observableArrayList(
         
            );
        final ComboBox comboBox = new ComboBox(options);
        
        comboBox.getItems().addAll(
           "0", "1", "2", "3", "4", "5", "6",
           "7", "8", "9", "10", "11", "12", 
           "13", "14", "15", "16", "17", "18", 
           "19", "20", "21", "22", "23", "24"
        );
           
comboBox.setVisibleRowCount(5);


    
        
        Button next = new Button("Next");
        next.getStyleClass().add("sleep_confirm");
        sleepBox.getChildren().addAll(sleepQuestion,comboBox,next);
        sleepBox.getStyleClass().add("sleep_box");
        sleepPrompt.setCenter(sleepBox);
        Scene primaryScene = new Scene(sleepPrompt);
        primaryStage.setScene(primaryScene);
        
        next.setOnMouseClicked(e ->{
            String afterHoursString = (String)(comboBox.getValue());
            int afterHours = Integer.parseInt(afterHoursString);
            
           
            BorderPane encouragement = new BorderPane();
            
            Scene myScene = new Scene(encouragement);        
            primaryStage.setScene(myScene);
            
            
            Label afterHoursPrompt = new Label(); 
            
            if(afterHours < 5) {
             afterHoursPrompt.setText("WHAT ARE YOU DOING?!??!???" 
                     + "\nCLOSE THIS APP IMMEDIATELY & GET SOME SLEEP!");
            
            }
            
            if(afterHours > 6) {
                afterHoursPrompt.setText("WOW!!!!! GOOD JOB!"
                        + "\nYou wil never have a baaaa-ad day with a good night's sleep!");
            }
            
            if(afterHours == 5 || afterHours == 6) {
                afterHoursPrompt.setText("Okay... there's room for improvement." 
                         + "\nPlease try to sleep more!");
            
            }
            VBox hoursPrompt = new VBox();
            hoursPrompt.setAlignment(Pos.CENTER);
            Button playGame = new Button("Next");
            
            hoursPrompt.getChildren().addAll(afterHoursPrompt,playGame);
            encouragement.setCenter(hoursPrompt);
            primaryStage.show();
            
            
            playGame.setOnMouseClicked(new EventHandler<MouseEvent>() {

                public void handle(MouseEvent e) {
                    gamePlayController.handleGameInit(primaryStage);
                }
            });
        });
    }
}
