/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OQ;

import OQ.view.OperationQView;
import java.io.File;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * 
 * @author BunnyRailgun
 */
public class OperationQ extends Application {
    
    OperationQView ui = new OperationQView();
    String appTitle = "DREAM IN SHEEP";
    
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        ui.startUI(primaryStage, appTitle);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
