/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OQ;

/**
 * Will contain constants
 * @author BunnyRailgun
 */
public class StartupConstants {
    
    public static String PATH_CSS = "/OQ/style/";
    public static String PATH_IMAGES = "/images/";
    public static String STYLE_SHEET_UI = PATH_CSS + "OperationQStyle.css";
    public static String TITLE_NAME = "SHEEP";
}
