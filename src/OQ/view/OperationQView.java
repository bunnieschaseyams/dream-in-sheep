/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OQ.view;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import OQ.StartupConstants;
import OQ.controller.TitleScreenController;

/**
 *
 * @author BunnyRailgun
 */
public class OperationQView {
    Stage primaryStage;
    Scene primaryScene;
    TitleScreenController titleScreenController = new TitleScreenController();
    
    //UI
    BorderPane titleScreen;
    
    //TOP OF BORDER
    FlowPane toolBar;
    Button about;
    
    //CENTER OF BORDER
    VBox titleButtons;
    Label titleName;
    Button startGame;
    Button settings;
    Button howToPlay;
    
    
    public void startUI(Stage initPrimaryStage, String appName){
        primaryStage = initPrimaryStage;
        initWindow(appName);      
    }    
    private void initWindow(String windowTitle) {
	// SET THE WINDOW TITLE
        primaryStage = new Stage();
	primaryStage.setTitle(windowTitle);
        

	// GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX()/2);
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth()/2);
	primaryStage.setHeight(bounds.getHeight());
        //Set UI
        titleScreen = new BorderPane();
        primaryScene = new Scene(titleScreen);
        primaryScene.getStylesheets().add(StartupConstants.STYLE_SHEET_UI);//SET STYLESHEET
        titleScreen.getStyleClass().add("title_screen");
        
        
        //TOP
        toolBar = new FlowPane();
        about = new Button("ABOUT");
        about.getStyleClass().add("title_button");
        toolBar.getChildren().add(about);
        
        //ENTER BUTTONS FOR TITLE HERE
        titleButtons = new VBox();
        titleButtons.getStyleClass().add("title_buttons_vbox");
        titleName = new Label(StartupConstants.TITLE_NAME);
        titleName.getStyleClass().add("title_name");
        howToPlay = new Button("HOW TO PLAY");
        howToPlay.getStyleClass().add("title_button");
        settings = new Button("SETTINGS");
        settings.getStyleClass().add("title_button");
        startGame = new Button("START GAME");
        startGame.getStyleClass().add("title_button");
        titleButtons.getChildren().addAll(titleName, howToPlay,settings,startGame);
        
        //SET TOP AND CENTER
        titleScreen.setTop(toolBar);
        titleScreen.setCenter(titleButtons);
        
        //SET SCENE AND SHOW
        primaryStage.setScene(primaryScene);
        primaryStage.show();

        //HANDLERS
        startGame.setOnMouseClicked(e->{
            titleScreenController.handleStartGame(primaryStage, startGame);
        });
    }
}
